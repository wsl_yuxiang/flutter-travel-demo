import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';

const APPBAR_SCROLL_OFFET = 100;

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List _imageUrls = [
    "assets/banner/banner1.jpg",
    "assets/banner/banner2.jpg",
  ];

  double appbarAlpha = 0;

  _onScroll(offet) {
    double alpha = offet / APPBAR_SCROLL_OFFET;
    if (alpha < 0) {
      alpha = 0;
    } else if (alpha > 1) {
      alpha = 1;
    }
    setState(() {
      appbarAlpha = alpha;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MediaQuery.removeViewPadding(
          removeTop: true, // 去掉设备上方的空白，flutter做了适配留白
          context: context,
          child: Stack(
            // 将改变透明度的widget 叠在上边
            children: <Widget>[
              NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollUpdateNotification &&
                      scrollNotification.depth == 0) {
                    // 滚动 而且是列表滚动的时候(第0哥元素 就是banner)
                    _onScroll(scrollNotification.metrics.pixels);
                  }
                  return true;
                },
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 180,
                      child: Swiper(
                        itemBuilder: (BuildContext context, int index) {
                          return Image.asset(
                            _imageUrls[index],
                            fit: BoxFit.cover,
                          );
                          // return Image(image: AssetImage("assets/banner/banner1.jpg"), width: 100.0);
                          // return Image.network("https://via.placeholder.com/350x150",fit: BoxFit.fill,);
                        },
                        itemCount: _imageUrls.length,
                        autoplay: true,
                        // 原点
                        pagination: const SwiperPagination(
                            builder: DotSwiperPaginationBuilder(
                                activeColor: Colors.blue)),
                        // 左右切换
                        // control: SwiperControl(),
                      ),
                    ),
                    const SizedBox(
                      height: 800,
                      child: ListTile(
                        title: Text('列表'),
                      ),
                    )
                  ],
                ),
              ),
              Opacity(
                opacity: appbarAlpha,
                child: Container(
                  height: 80,
                  decoration: const BoxDecoration(color: Colors.white),
                  child: const Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text('首页'),
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
