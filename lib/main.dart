import 'package:flutter/material.dart';

import 'navigator/tab_navigator.dart';

// 入口函数
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  // Widget 是每一个组件
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
       localizationsDelegates: const [
        // GlobalMaterialLocalizations.delegate,
        // DefaultMaterialLocalizations.delegate,
        // GlobalWidgetsLocalizations.delegate,
        // DefaultWidgetsLocalizations.delegate,
        // GlobalCupertinoLocalizations.delegate,
        // DefaultCupertinoLocalizations.delegate
      ],
      theme: ThemeData(
        // This is the theme of your application.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const TabNavigator(),
    );
  }
}