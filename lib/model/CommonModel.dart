class CommonModel {
  final int userId;
  final int id;
  final String title;
  final String body;

  CommonModel(
      {required this.userId,
      required this.id,
      required this.title,
      required this.body});

  factory CommonModel.fromJson(Map<String, dynamic> json) {
    return CommonModel(
        userId: json['userId'],
        id: json['id'],
        title: json['title'],
        body: json['body']);
  }
}
