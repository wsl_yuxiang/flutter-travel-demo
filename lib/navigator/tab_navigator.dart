import 'package:flutter/material.dart';
import 'package:myflutterdemo/pages/home_page.dart';
import 'package:myflutterdemo/pages/my_page.dart';
import 'package:myflutterdemo/pages/search_page.dart';
import 'package:myflutterdemo/pages/travel_page.dart';

class TabNavigator extends StatefulWidget {
  const TabNavigator({super.key});

  @override
  _TabNavigatorState createState() => _TabNavigatorState();
}

class _TabNavigatorState extends State<TabNavigator> {
  final PageController _pageController =
      PageController(initialPage: 0); // 初始状态为第一个页面

  final Color _defaultColor = Colors.black;
  final Color _activeColor = Colors.blue;

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        onPageChanged: (value) => {
            setState(() {
            _currentIndex = value;
          })
        },
        children: const <Widget>[HomePage(), SearchPage(), TravelPage(), MyPage()],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          _pageController.jumpToPage(index);
          setState(() {
            _currentIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed, // 固定显示 不需要放大
        selectedItemColor: _activeColor,
        unselectedItemColor: _defaultColor,
        selectedIconTheme: IconThemeData(color: _activeColor),
        unselectedIconTheme: IconThemeData(color: _defaultColor),
        items: const [
          BottomNavigationBarItem(
            label: '首页',
            icon: Icon(
              Icons.home,
            ),
          ),
          BottomNavigationBarItem(
            label: '搜索',
            icon: Icon(
              Icons.search,
            ),
          ),
          BottomNavigationBarItem(
              label: '旅拍',
              icon: Icon(
                Icons.camera,
              ),
              ),
          BottomNavigationBarItem(
              label: '我的',
              icon: Icon(
                Icons.person,
              ),
             )
        ],
      ),
    );
  }
}
